# README

Agile Proxy Client is a nodejs javascript client for the 'Agile Proxy Server' (https://github.com/garytaylor/agileproxy.git).

## Installation

npm install --save-dev agile-proxy-client

## Usage

### To define proxy stubs
    var proxy;
    proxy = require('agile-proxy-client');
    proxy.define([
        proxy.stub('http://www.google.com', {method: 'GET'}).andReturn({text: 'I am not google'});
        proxy.stub('http://mydomain.com/users', {method: 'POST'}).andReturn({json: {user: {name: 'Test User', id: 1}}});
    ]);

### To configure
    var proxy;
    proxy = require('agile-proxy-client');
    proxy.config({proxyUrl: 'http://localhost:4040', restUrl: 'http://localhost:4041', username: 'testuser', apiKey: 'fghjkjhgfffghjklkjhgf'});
