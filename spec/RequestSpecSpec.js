var _, proxyquire;
_ = require('underscore');
proxyquire = require('proxyquire');
describe('RequestSpec', function () {
    var RequestSpec, subject, MockResponse, MockRequest;
    MockResponse = function MockResponse(attrs) {
        _.extend(this, attrs);
    }
    _.extend(MockResponse.prototype, {
        asJson: function () {return {text: this.text}; }
    });


    beforeEach(function () {
        MockRequest = jasmine.createSpyObj('request', ['get', 'post']);
        RequestSpec = proxyquire('../agile-proxy-client/RequestSpec', {'./Response': MockResponse, 'request': MockRequest});
    });
    it('Should store an attributes specified in the constructor in the instance', function () {
        subject = new RequestSpec({url: 'someUrl', method: 'GET'});
        expect(subject.url).toEqual('someUrl');
        expect(subject.method).toEqual('GET');
    });
    it('Should create a new response when the andReturn method is called', function () {
        subject = new RequestSpec({url: 'someUrl', method: 'GET'});
        subject.andReturn({text: 'Hello World', contentType: 'text/html'});
        expect(subject.response).toEqual(jasmine.any(MockResponse));
        expect(subject.response.text).toEqual('Hello World');
        expect(subject.response.contentType).toEqual('text/html');
    });
    describe('Saving to the server', function () {
        var callback, scope, restUrl;
        beforeEach(function () {
            restUrl = 'http://localhost/v1/users/1/applications/1/request_specs';
            callback = jasmine.createSpy('callback');
            scope = {name: 'My Scope'};
            subject = new RequestSpec({url: 'someUrl', method: 'GET'});
            subject.andReturn({text: 'Hello World'});
        });
        it('Should save to the server when the done method is called', function () {
            var args;
            subject.done(restUrl, callback, scope);
            expect(MockRequest.post).toHaveBeenCalledWith('http://localhost/v1/users/1/applications/1/request_specs', {json: {request_spec: {url: 'someUrl', http_method: 'GET', response: {text: 'Hello World'}}}}, jasmine.any(Function));
            args = MockRequest.post.calls.mostRecent().args;
            args[2].apply(MockRequest, [null, {statusCode: 200},JSON.stringify({mock_request: {id: 'id1'}})]);
            expect(callback).toHaveBeenCalledWith(null, {id: 'id1'});
        });
        it('Should save to the server when the done method is called but the callback should receive an exception if there is a server error', function () {
            var args;
            subject.done(restUrl, callback, scope);
            expect(MockRequest.post).toHaveBeenCalledWith('http://localhost/v1/users/1/applications/1/request_specs', {json: {request_spec: {url: 'someUrl', http_method: 'GET', response: {text: 'Hello World'}}}}, jasmine.any(Function));
            args = MockRequest.post.calls.mostRecent().args;
            args[2].apply(MockRequest, [new Error('Not Found'), {statusCode: 404}, '']);
            expect(callback).toHaveBeenCalledWith(new Error('Not Found'), '');
        });
    });

});