var _;
_ = require('underscore');
describe('The proxy', function () {
    var proxy, MockRequestSpec, proxyquire;
    beforeEach(function () {
        proxyquire = require('proxyquire');
        MockRequestSpec = function MockRequestSpec() {};
        _.extend(MockRequestSpec, {
            config: function () {},
            stub: function () {
                return this;
            },
            andReturn: function () {
                return this;
            },
            done: function () {}
        });
    });
    beforeEach(function () {
        proxy = proxyquire('..', {'./RequestSpec': MockRequestSpec});
        proxy.config({
            proxyUrl: 'http://localhost:4040',
            restUrl: 'http://localhost:4041',
            username: 'testuser',
            apiKey: 'apikey'
        });
    });
    describe('With 2 stubs', function () {
        var stubs, callback, restUrl;
        beforeEach(function () {
            stubs = [
                proxy.stub('http://www.google.com', {method: 'GET'}).andReturn({text: 'I am not google'}),
                proxy.stub('http://mydomain.com/users', {method: 'POST'}).andReturn({json: {user: {name: 'Test User', id: 1}}})
            ];
            spyOn(stubs[0], 'done');
            spyOn(stubs[1], 'done');
            callback = jasmine.createSpy('callback');
            restUrl = 'http://localhost:4041/v1/users/1/applications/1/request_specs';
        });
        it('Should respond to the define method with an array of objects to send to the server by calling the done method on all.', function () {
            proxy.define(stubs, callback);
            expect(stubs[0].done).toHaveBeenCalledWith(restUrl, jasmine.any(Function), jasmine.any(Object));
            expect(stubs[1].done).toHaveBeenCalledWith(restUrl, jasmine.any(Function), jasmine.any(Object));
            expect(callback).not.toHaveBeenCalled();
            stubs[0].done.calls.mostRecent().args[1].apply(this, []);
            expect(callback).not.toHaveBeenCalled();
            stubs[1].done.calls.mostRecent().args[1].apply(this, []);
            expect(callback).toHaveBeenCalledWith(null);

        });
        it('Should not fire the callback to define if one of the callbacks comes back with an error', function () {
            proxy.define(stubs, callback);
            expect(stubs[0].done).toHaveBeenCalledWith(restUrl, jasmine.any(Function), jasmine.any(Object));
            expect(stubs[1].done).toHaveBeenCalledWith(restUrl, jasmine.any(Function), jasmine.any(Object));
            expect(callback).not.toHaveBeenCalled();
            stubs[0].done.calls.mostRecent().args[1].apply(this, []);
            expect(callback).not.toHaveBeenCalled();
            stubs[1].done.calls.mostRecent().args[1].apply(this, [new Error('Not Found'), '']);
            expect(callback).toHaveBeenCalledWith(new Error('Not Found'));
        });
    });
});