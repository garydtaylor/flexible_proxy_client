var _;
_ = require('underscore');
describe('The response', function () {
    var response, Response;
    beforeEach(function () {
        Response = require('../agile-proxy-client/Response');
    });
    it('Return a snake cased json representation with asJson', function () {
        response = new Response({content: '{"key": "value"}', contentType: 'application/json'});
        expect(response.asJson()).toEqual({content: '{"key": "value"}', content_type: 'application/json'});
    });
    it('Return a snake cased json representation when used with "body"', function () {
        response = new Response({body: '{"key": "value"}', contentType: 'application/json'});
        expect(response.asJson()).toEqual({content: '{"key": "value"}', content_type: 'application/json'});
    });
    it('Return a snake cased json representation when used with "text"', function () {
        response = new Response({text: '{"key": "value"}'});
        expect(response.asJson()).toEqual({content: '{"key": "value"}', content_type: 'text/plain'});
    });
    it('Return a snake cased json representation when used with "json"', function () {
        response = new Response({json: {"key": "value"}});
        expect(response.asJson()).toEqual({content_type: 'application/json', content: '{"key":"value"}'});
    });
    it('Return a snake cased json representation when used with "html"', function () {
        response = new Response({html: "Some random text"});
        expect(response.asJson()).toEqual({content_type: 'text/html', content: 'Some random text'});
    });
    it('Return a snake cased json representation when used with "textTemplate"', function () {
        response = new Response({textTemplate: '{"key": "{{value}}"}'});
        expect(response.asJson()).toEqual({content: '{"key": "{{value}}"}', content_type: 'text/plain', is_template: true});
    });
    it('Return a snake cased json representation when used with "jsonTemplate"', function () {
        response = new Response({jsonTemplate: {"key": "{{value}}"}});
        expect(response.asJson()).toEqual({content_type: 'application/json', content: '{"key":"{{value}}"}', is_template: true});
    });
    it('Return a snake cased json representation when used with "htmlTemplate"', function () {
        response = new Response({htmlTemplate: "Some random {{text}}"});
        expect(response.asJson()).toEqual({content_type: 'text/html', content: 'Some random {{text}}', is_template: true});
    });


});
