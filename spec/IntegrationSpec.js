var _;
_ = require('underscore');
describe('The integration test', function () {
    var proxy, restPort, spec1Request, spec2Request, freeport;
    beforeEach(function (done) {
        freeport = require('freeport');
        freeport(function (er, port) {
            if (!er) {
                restPort = port;
                done();
            }
        });
    });
    beforeEach(function () {
        var nock = require('nock');
        spec1Request = nock('http://localhost:' + restPort)
            .post('/v1/users/1/applications/1/request_specs')
            .reply(200, {});
        spec2Request = nock('http://localhost:' + restPort)
            .post('/v1/users/1/applications/1/request_specs')
            .reply(200, {});
    });
    beforeEach(function () {
        proxy = require('agile-proxy-client');
    });
    beforeEach(function () {
        proxy.config({
            proxyUrl: '',
            restUrl: 'http://localhost:' + restPort
        });
    });
//    it('Should send the request specs to the server on define', function (done) {
//        proxy.define([
//            proxy.stub('http://www.google.com', {method: 'GET'}).andReturn({text: 'I am not google'}),
//            proxy.stub('http://mydomain.com/users', {method: 'POST'}).andReturn({json: {user: {name: 'Test User', id: 1}}})
//        ], function () {
//            done();
//        });
//    });

});